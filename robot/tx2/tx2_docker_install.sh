#!/bin/bash
## docker
sudo apt-get update && sudo apt-get upgrade && sudo apt-get -y install curl apt-transport-https

### install docker-ce
# root required for the following, sudo not working
sudo su -c "echo 'deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu xenial edge' > /etc/apt/sources.list.d/docker.list; curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -qq -> /dev/null;"
sudo apt-get update && sudo apt-get -y install docker-ce

### enable docker service
sudo systemctl enable docker

### add user to docker group
sudo gpasswd -a $USER docker
