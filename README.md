# NeuroRace Dockerfiles
To simplify deployment [docker](https://www.docker.com/) is used. For instructions how to install and handle docker, please refer to its documentation.  
All container images are published on [https://hub.docker.com/u/neurorace](https://hub.docker.com/u/neurorace). 

## On this page
- [Dockerfiles](#dockerfiles)
  - [Desktop - Ubuntu 18.04 (Bionic)](#desktop-ubuntu-18-04-bionic)
    - [Runable Demo](#neuroracesimulation-full-runable-demo-including-pretrained-models) 
  - [Robot - Jetson TX 2 (JetPack v3.3 flash required)](#robot-jetson-tx-2-jetpack-v33-flash-required)
- [Native Building](#native-building)

## Dockerfiles
The provided Dockerfiles contain different setups 
* ___base___ - the basic software stack
* ___full___ - base + neuroracer packages including a runable setup for training and pretrained models + guideline
* ___dev___ - developing version + guideline

__CUDA__ - ___True___, means GPU support is available.  
__OpenGL__ - ___True___, means it is possible to run 3D accelerated programs (like Gazebo) inside the container.  

There are different run setups for each, desktop and jetson. Netherless, for running GUI programs within Container in general, e.g. Gazebo, additional permissions are required. The lazy and reckless can just execute the following (follow the link for the safer way - and you really should: [https://wiki.ros.org/docker/Tutorials/GUI](https://wiki.ros.org/docker/Tutorials/GUI))
```bash
$ xhost +local:root
```

**Note:**
* `The full version of each, simulation and robot, includes all AI packages`
* `Simulation` images are `x86_64` based
* `Robot` images are `ARMv8` based (Jetson TX2)

Since training is independent of execution and vice versa, you can actually train on desktop or robot itself and simple execute on the opposite afterwards. Of course, for supervised learning tasks it is advised to train on the more powerful hardware.

### Desktop - Ubuntu 18-04 (Bionic)
Due to Gazebo, a dedicated graphic card is recommended. In this project, NVIDIA hardware is used. To have GPU acceleration inside docker, the usage of [`NVIDIA Container Toolkit`](https://github.com/NVIDIA/nvidia-docker)` is recommended` for the simulation (GPU is also used for data processing and machine learning tasks).

#### Features
* __CUDA__ - ___True___
* __OpenGL__ - ___True___

#### Dockerfiles
##### [neurorace/simulation-base](desktop/ubuntu_18_04/base)
```bash
docker run -it \
    --env="DISPLAY=$DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --env="XAUTHORITY=$XAUTH" \
    --volume="$XAUTH:$XAUTH" \
    --gpus all \
    --rm \
    neurorace/simulation-base \
    bash
```

##### [neurorace/simulation-full](desktop/ubuntu_18_04/full) (runable demo including pretrained models)
**`Coming soon`**

##### [neurorace/simulation-dev](desktop/ubuntu_18_04/dev)
**`Coming soon`**


### Robot - Jetson TX 2 (JetPack v3.3 flash required)
The Jetson TX2 needs to be flashed by JetPack v3.3 with the packages as described in [neuroracer-robot-install - Prerequisites](https://gitlab.com/NeuroRace/neuroracer-robot-install#prerequisites) and additionally for controller and wireguard support, the custom kernel build script should be executed.  
Afterwards, to install and run docker on preflashed Jetson TX2, execute [tx2_docker_install.sh](robot/tx2/tx2_docker_install.sh) script, reboot and run your chosen docker image.

#### Features
* __CUDA__ - ___True___
* __OpenGL__ - _`False`_

#### Dockerfiles
##### [neurorace/robot-base](robot/tx2/base)
```bash
docker run -it --rm \
    --privileged \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    neurorace/robot-base \
    bash
```

##### [neurorace/robot-full](robot/tx2/full)
**`Coming soon`**

##### [neurorace/robot-dev](robot/tx2/dev)
**`Coming soon`**


## Native Building
To build any neurorace images locally on your machine, clone this repository, navigate to your chosen docker file and execute

```bash
docker build -t yourtag .
```
